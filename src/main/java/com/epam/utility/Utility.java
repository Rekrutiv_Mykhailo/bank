package com.epam.utility;

public class Utility {
    public static final String CREATE_BANK = "INSERT bank (name_bank, balance, transaction_percent, id_currency, id_credential) VALUES (?,?,?,?,?)";
    public static final String UPDATE_BANK = "UPDATE bank SET   name_bank=?, balance=?, transaction_percent=?, id_currency=?, id_credential=? where id_bank=?";
    public static final String FIND_ALL_BANK = "select b.id_bank, b.name_bank, b.balance, b.transaction_percent, b.id_currency, c.name_currency, c.exchange_rate, cr.id_credential,  cr.password, cr.login from bank as b inner join currency as c on c.id_currency=b.id_currency inner join credential as cr on cr.id_credential=b.id_credential";
    public static final String FIND_BY_ID_BANK = "select b.id_bank, b.name_bank, b.balance, b.transaction_percent, b.id_currency, c.name_currency, c.exchange_rate, cr.id_credential,  cr.password, cr.login from bank as b inner join currency as c on c.id_currency=b.id_currency and b.id_bank = ? inner join credential as cr on cr.id_credential=b.id_credential";
    public static final String FIND_BY_NAME_BANK = "select b.id_bank, b.name_bank, b.balance, b.transaction_percent, b.id_currency, c.name_currency, c.exchange_rate, cr.id_credential,  cr.password, cr.login from bank as b inner join currency as c on c.id_currency=b.id_currency and b.name_bank = ? inner join credential as cr on cr.id_credential=b.id_credential";
    public static final String DELETE_BANK = "DELETE FROM bank WHERE id_bank=?";

    public static final String FIND_ONE_BY_LOGIN_CREDENTIAL = "SELECT * FROM credential where login=? limit 1";
    public static final String DELETE_CREDENTIAL = "DELETE FROM credential WHERE id_credential=?";
    public static final String UPDATE_CREDENTIAL = "UPDATE credential SET login=?, password=? WHERE id_credential=?";
    public static final String CREATE_CREDENTIAL = "INSERT credential (login,password) VALUES (?, ?)";

    public static final String FIND_ALL_CURRENCY = "select * from currency";
    public static final String FIND_ONE_BY_NAME_CURRENCY = "select *   from currency where name_currency=?";
    public static final String FIND__BY_ID_CURRENCY = "select *   from currency where id_currency=?";
    public static final String DELETE_CURRENCY = "DELETE FROM currency WHERE id_currency=?";
    public static final String UPDATE_CURRENCY = "UPDATE currency SET name_currency=?, exchange_rate=?, WHERE id=?";
    public static final String CREATE_CURRENCY = "INSERT user (name_currency,exchange_rate) VALUES (?, ?)";

    public static final String FIND_ALL_USER = "select u.id_user, u.name_user, u.surname,u.type_user,c.id_credential,c.login,c.password  from user as u inner join credential as c  on u.id_credential=c.id_credential";
    public static final String FIND_ONE_BY_LOGIN_USER = "select u.id_user, u.name_user, u.surname,u.type_user,c.id_credential,c.login,c.password  from user as u inner join credential as c  on u.id_credential=c.id_credential and c.login=?";
    public static final String DELETE_USER = "DELETE FROM user WHERE id_user=?";
    public static final String UPDATE_USER = "UPDATE credential SET name_user=?, surname=?, type_user=?, id_credential=? WHERE id_user=?";
    public static final String CREATE_USER = "INSERT user (name_user,surname,type_user,id_credential) VALUES (?, ?, ?, ?)";

    public static final String FIND_BY_USER="select c.id_card, c.number, c.balance, c.id_bank, c.id_user, u.name_user, u.surname, u.type_user, u.id_credential from card as c inner join user as u on c.id_user=u.id_user and u.id_user=?";



}
