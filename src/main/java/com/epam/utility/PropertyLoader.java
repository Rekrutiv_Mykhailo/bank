package com.epam.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
    private Properties properties;

    public PropertyLoader() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream("src\\main\\resources\\conf.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Properties getProperties() {
        return properties;
    }
}
