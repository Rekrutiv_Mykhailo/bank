package com.epam.dao.impl;

import com.epam.connector.ConnectionManager;
import com.epam.dao.CurrencyDao;
import com.epam.model.entity.Currency;
import com.epam.parser.Transformer;
import static com.epam.utility.Utility.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CurrencyDaoImpl implements CurrencyDao {

    @Override
    public Currency findByName(String name) throws SQLException {
        Currency currency = new Currency();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ONE_BY_NAME_CURRENCY)) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    currency = (Currency) new Transformer(Currency.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return currency;
    }

    @Override
    public Currency findById(Integer id) throws SQLException {
        Currency currency = new Currency();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND__BY_ID_CURRENCY)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    currency = (Currency) new Transformer(Currency.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return currency;
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE_CURRENCY)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Currency currency) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE_CURRENCY)) {
            ps.setInt(3, currency.getId());
            ps.setString(1, currency.getNameCurrency());
            ps.setInt(2, currency.getExchangeRate());
            return ps.executeUpdate();
        }

    }

    @Override
    public List<Currency> findAll() throws SQLException {
        List<Currency> currencies = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL_CURRENCY)) {
                while (resultSet.next()) {
                    currencies.add((Currency) new Transformer(Currency.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return currencies;
    }
}
