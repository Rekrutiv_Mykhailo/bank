package com.epam.dao.impl;

import com.epam.connector.ConnectionManager;
import com.epam.dao.CredentialDao;
import com.epam.model.entity.Credential;
import com.epam.parser.Transformer;
import static com.epam.utility.Utility.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class CredentialDaoImpl implements CredentialDao {

    @Override
    public Credential findByLogin(String login) throws SQLException {
        Credential credential = new Credential();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ONE_BY_LOGIN_CREDENTIAL)) {
            preparedStatement.setString(1, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()) {
            credential = (Credential) new Transformer(Credential.class).fromResultSetToEntity(resultSet);
            }
            }
        }
        return credential;
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE_CREDENTIAL)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Credential credential) throws SQLException{
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE_CREDENTIAL)) {
            ps.setInt(3, credential.getId());
            ps.setString(1,credential.getLogin());
            ps.setString(2,credential.getPassword());
            return ps.executeUpdate();
        }
    }

    @Override
    public int create(Credential credential) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE_CREDENTIAL)) {
            ps.setString(1,credential.getLogin());
            ps.setString(2,credential.getPassword());
            return ps.executeUpdate();
        }
    }
}
