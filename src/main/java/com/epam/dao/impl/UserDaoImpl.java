package com.epam.dao.impl;

import com.epam.connector.ConnectionManager;
import com.epam.dao.UserDao;
import com.epam.model.entity.User;
import com.epam.parser.Transformer;
import static com.epam.utility.Utility.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    @Override
    public List<User> findAll() throws SQLException {
        List<User> users = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL_USER)) {
                while (resultSet.next()) {
                    users.add((User) new Transformer(User.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return users;
    }

    @Override
    public User findByLogin(String login) throws SQLException {
        User user;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ONE_BY_LOGIN_USER)) {
            preparedStatement.setString(1, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                user = (User) new Transformer(User.class).fromResultSetToEntity(resultSet);
            }
        }
        return user;
    }

    @Override
    public int update(User user) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE_USER)) {
            ps.setInt(5, user.getId_user());
            return fillPreparedStatement(user, ps);
        }
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE_USER)) {
            ps.setInt(1, id);
            return ps.executeUpdate();

        }
    }

    public int create(User user) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE_USER)) {
            return fillPreparedStatement(user, ps);
        }
    }

    private int fillPreparedStatement(User user, PreparedStatement ps) throws SQLException {
        ps.setString(1, user.getName());
        ps.setString(2, user.getSurname());
        ps.setString(3, user.getTypeUser());
        ps.setInt(4, user.getCredential().getId());
        return ps.executeUpdate();
    }
}
