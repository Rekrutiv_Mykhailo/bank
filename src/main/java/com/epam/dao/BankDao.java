package com.epam.dao;

import com.epam.model.entity.Bank;

import java.sql.SQLException;
import java.util.List;

public interface BankDao {
    Bank findByName(String name) throws SQLException;

    Bank findById(Integer id) throws SQLException;

    int deleteById(Integer id) throws SQLException;

    int update(Bank bank) throws SQLException;

    List<Bank> findAll() throws SQLException;

    int create(Bank bank) throws SQLException;
}
