package com.epam.dao;

import com.epam.model.entity.Card;
import com.epam.model.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface CardDao {
    List<Card> findByUser(Integer userId)throws SQLException;
    List<Card> findByBank(Integer bankId)throws SQLException;
    Card findByID(Integer id)throws SQLException;
    int deleteById(Integer cardId)throws SQLException;
    int create(Card card)throws SQLException;
    int update(Card card)throws SQLException;
}
