package com.epam.model.entity;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.ObjectColumn;
import com.epam.model.Annotation.Table;

import java.util.Objects;

@Table(name = "bank")
public class Bank {
    @Column(name = "id_bank")
    private Integer id;
    @Column(name = "name_bank")
    private String name;
    @Column(name = "balance")
    private Integer balance;
    @Column(name = "transaction_percent")
    private Integer transactionPercent;
    @ObjectColumn
    Currency currency;
    @ObjectColumn
    Credential credential;

    public Bank() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getTransactionPercent() {
        return transactionPercent;
    }

    public void setTransactionPercent(Integer transactionPercent) {
        this.transactionPercent = transactionPercent;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", transactionPercent=" + transactionPercent +
                ", currency=" + currency +
                ", credential=" + credential +
                '}';
    }
}
