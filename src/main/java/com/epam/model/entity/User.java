package com.epam.model.entity;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.ObjectColumn;
import com.epam.model.Annotation.Table;

@Table(name = "user")
public class User {
    @Column(name = "id_user")
    private Integer id_user;
    @Column(name = "name_user")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "type_user")
    private String typeUser;
    @ObjectColumn
    Credential credential;

    public User() {
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String tupeUser) {
        this.typeUser = tupeUser;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public String toString() {
        return "User{" +
                "id_user=" + id_user +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", typeUser='" + typeUser + '\'' +
                ", credential=" + credential +
                '}';
    }
}
