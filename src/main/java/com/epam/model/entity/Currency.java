package com.epam.model.entity;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.Table;

import java.util.Objects;

@Table(name = "currency")
public class Currency {
    @Column(name = "id_currency")
    private Integer id;
    @Column(name = "name_currency")
    private String nameCurrency;
    @Column(name = "exchange_rate")
    private Integer exchangeRate;

    public Currency() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameCurrency() {
        return nameCurrency;
    }

    public void setNameCurrency(String nameCurrency) {
        this.nameCurrency = nameCurrency;
    }

    public Integer getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Integer exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(id, currency.id) &&
                Objects.equals(nameCurrency, currency.nameCurrency) &&
                Objects.equals(exchangeRate, currency.exchangeRate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameCurrency, exchangeRate);
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", nameCurrency='" + nameCurrency + '\'' +
                ", exchangeRate=" + exchangeRate +
                '}';
    }
}
