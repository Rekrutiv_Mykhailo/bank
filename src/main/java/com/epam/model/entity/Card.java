package com.epam.model.entity;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.ObjectColumn;
import com.epam.model.Annotation.Table;

@Table(name = "card")
public class Card {
    @Column(name = "id_card")
    private Integer id;
    @Column(name = "number")
    private String number;
    @Column(name = "balance")
    private Integer balance;
    @Column(name = "id_bank")
    private Integer id_bank;
    @ObjectColumn
    private User user;

    public Card() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", balance=" + balance +
                ", id_bank=" + id_bank +
                ", user=" + user +
                '}';
    }
}

