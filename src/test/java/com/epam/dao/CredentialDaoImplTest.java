package com.epam.dao;

import com.epam.dao.impl.CredentialDaoImpl;
import com.epam.model.entity.Credential;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.Logger;

import java.sql.SQLException;
import java.util.logging.LogManager;

class CredentialDaoImplTest {
    private static CredentialDao credentialDao;


    @BeforeAll
    static void init() {
        credentialDao = new CredentialDaoImpl();
    }

    @Test
    void testCreateAndFindByLogin() throws SQLException {
        Credential credential = new Credential("login", "password");
        credentialDao.create(credential);
        Credential credential1 = credentialDao.findByLogin("login");
        Assertions.assertEquals(credential.getLogin(), credential1.getLogin());
    }

    @Test
    void deleteById() throws SQLException {
        assert credentialDao.deleteById(3)!=1;
    }
}
